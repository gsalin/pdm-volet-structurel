# PDM-Volet-Structurel Centre toulouse 2023

## Fichier des adresses personnelles
Pour le fichier toulousain extrait par le service "administration locale du personnel" ([cf besoins](https://forgemia.inra.fr/gsalin/pdm-volet-structurel/-/issues/1)): 
- 16 adresses exclues pour les personne ne souhaitant pas que leurs données personnées soient utilisées
- 1414 adresses restantes

L'objectif est de mettre en place une procédure automatisée de geocoding (transformation des adresses personnelles en point GPS) sur le fichier excel, à l'aide d'une macro et d'un appel à des API (Application Programming Interface) de traitement.
2 API ont été identifiées, outils libres et respectueux des données personnelles pour faire cette conversion : 
- [Nominatim](https://nominatim.openstreetmap.org/) qui repose sur le projet open source contributif [openstreetmap](https://www.openstreetmap.org/about)
- [IGN](https://geoservices.ign.fr/documentation/services/api-et-services-ogc/geocodage-20/doc-technique-api-geocodage) qui repose sur la base de données de l'Institut Géographique National

Les tests des API se font sur le fichier excel des adresses personnelles des agents dans le quel des macros en VBA ont été implémentées de façon à faire appel à ces API de façon très simple.

### Test API Nominatim
Après 1 premier geocoding avec l'API Nominatim sur l'adresse complète non normalisée : 
- 347 adresses sont non trouvées

Normalisation des adresses après quelques tests manuels montrant un meilleur résultat : 
- remplacement chem par chemin
- remplacement all par allee
- remplacement rte par route
- remplacement Bd. par boulevard
- remplacement Bld. par boulevard
- remplacement des caractères accentués par leur équivalent sans accent
- correction de noms de communes erronés (Toulouse à la place de Tolouse)
- suppression des espaces superflus (double, triple, quadruple.....) dans les colonnes Matricule et Adresse
- ....


Relance du traitement avec l'API nominatim sur l'adresse complète **normalisée**: 
- restent 248 adresses non trouvées (82,5% d'adresses converties, contre 75,5% avant normalisation)

**La normalisation des adresses facilite la conversion en point GPS** > à automatiser dans la macro

Recherche manuelle en tâtonnant sur https://nominatim.openstreetmap.org/ui/search.html ou via google maps et recupération de la position GPS à partir des 248 adresses nominatim non trouvées : 
- restent 54 adresses non trouvées

Relance du traitement nominatim en n'utilisant que le code postal et le nom de la commune sur les 54 adresses nominatim non trouvées (résultat forcément moins précis, surtout dans le cas de grandes villes)
- restent 5 adresses non trouvées (on ne peut pas faire mieux, il n'y a pas de commune spécifiée)

### Test API IGN

Après 1 premier geocoding avec l'API IGN sur l'adresse complète **normalisée** : 
**- 6 adresses sont non trouvées (99,6% d'adresses converties)**

**Dans l'utilisation, l'appel à l'API IGN a quelquefois posé des problèmes de temps de réponse trop longs, obligeant à relancer les traitements** > permettre de lancer la macro de façon flexible en spécifiant le numéro de ligne où commencer et le nombre d'adresses à convertir. On va faire en sorte de conserver la possibilité d'appeler les 2 API de façon indépendante, via des boutons dédiés.

Recherche manuelle en tâtonnant sur https://nominatim.openstreetmap.org/ui/search.html ou via google maps et recupération de la position GPS à partir des 6 adresses IGN non trouvées : 
- restent 5 adresses non trouvées (on ne peut pas faire mieux, il n'y a pas de commune spécifiée)


### Conclusion pour le fichier des adresses personnelles  

L'API IGN est bien meilleure en nombre d'adresses converties, et semble donner des résultats plus exacts (souvent l'API nominatim donne un point GPS correspondant à la rue, et pas à la position exacte de l'habitat de l'agent)
- 475 points GPS correspondant à une rue sur 1164 adresses (40%) pour nominatim
- 111 points GPS correspondant à une rue sur 1406 adresses (8%) pour IGN

Pour l'implémentation de la macro Excel :   
- on privilégie l'API IGN, tout en permettant de faire un appel à l'API nominatim (dans le cas où l'API IGN soit innaccessible)
- faire un macro flexible pour ne pas retraiter l'ensemble des centaines d'adresses, mais uniquemet celles qu'on veut cibler (en cas d'erreur intervenue en cours d'analyse, pour faire des tests...)
- on impose une attente d'une seconde entre le traitement de 2 adresses (obligation pour nominatim sous peine de se faire exclure du service, pour ne pas trop stresser le serveur mutualisé, donc l'accès est gratuit - on utilisera le même protocole pour l'API IGN)


**Quelques difficultés sur ce fichier**
- agents avec une très vielle adresse postale (cas d'un collègue qui habite la région toulousaine depuis plusieurs années, mais dont l'adresse postale retournée dans le fichier est celle de ses parents - première adresse rentrée dans le système : **comment sont mises à jour les adresses? qui doit faire la demande à qui?**

## Fichier des lieux de travail
Pour le fichier toulousain extrait par le service "administration locale du personnel" : 
- 16 lignes exclues pour les personne ne souhaitant pas que leurs données personnées soient utilisées
- 3463 lignes restantes
A noter : ce fichier contient uniquement les personnes rattachées à une unité du centre INRAE de Toulouse (quel que soit le site), quel que soit le lieu d'exercice

Supprimer les espaces dans le champs Matricule

Il y a beaucoup de matricules doublonnés, correspondant à des contrats différents. On peut trier par matricule et Date d'entrée pour supprimer toutes les lignes les plus anciennes. Pour faciliter ce traitement, on peut mettre en surbrillance les doublons dans Excel : 
- sélectionner la colonne Matricule
- Accueil > Mise en forme conditionnelle > Règles de mise en surbrillance des cellules > Valeurs en double > Valider > Valider
- les lignes en double seront plus facilement repérables

Après suppression des doublons
- 3332 lignes restantes

Analyse des lieux de travail : 
- 24 lieux de travail uniques recensés
- 16 lieux de travail ne sont pas sur le centre INRAE de Toulouse (soit 66%)
- ramené pour chaque ligne : 
    - 46 lignes correspondent à des lieux de travail en dehors du centre INRAE de Toulouse
    - 3286 lignes correspondent à des lieux de travail sur le centre INRAE de Toulouse

Identification des lieux de travail uniques correspondant au centre de Toulouse
- copier les lieux de travail unique dans un nouvel onglet 
    - ajouter une colonne pour indiquer si le site est sur le centre de Toulouse
    - ajouter une colonne pour indiquer la position GPS du lieu de travail
- Pour chaque site
    - si le site fait partie du centre de Toulouse
		- mettre 1 dans la colonne précédente
		- mettre la position GPS dans la colonne précédente, sous la forme latitude;longitude
    - si le site ne fait pas partie du centre de Toulouse
        - mettre 1 dans la colonne précédente
        
Sélection des agents travaillant sur le site de Toulouse
- ajouter une colonne "GPS Lieu de travail" dans l'onget contenant les 3332 lignes
- dans cette colonne, appliquer la formule RECHERCHEV pour ajouter la position GPS pour le lieu de travail (si le lieu de travail fait partie du centre de Toulouse, une position GPS sera affichée, sinon il n'y aura rien)
- trier sur la colonne "GPS Lieu de travail" et supprimer les lignes qui ne contiennent pas de position
    

**Quelques difficultés sur ce fichier**
- On liste les personnes rattachées à une unité du centre de Toulouse, même si le lieu de travail est sur un autre centre (e.g. : DSI infra avec des personnes travaillant sur Avignon) > A exclure
- sont absentes du fichier les personnes rattachées à une unité d'un autre centre, mais travaillant physiquement sur le centre INRAE de Toulouse (3 personnes sur les 16 qui ne souhaitaient pas qu'on utilise leurs données perso sont dans ce cas, e.g. DRH Direction Ressources Humaines)
- personnes présentes dans ce fichier, mais absent du fichier des adresses perso et de l'annuaire (PEPS?)


## Fusion des fichiers des adresses personnelles et des lieux de travail
- créer un nouveau fichier excel FusionAdressesPersoEtPro.xlsm avec un onglet "AdressesPerso" et un onglet "AdressesPro"
- copier le contenu du fichier des adresses personnelles dans l'onglet AdressesPerso
- copier le contenu du fichier des lieux de travail dans AdressesPro
- dans l'onglet AdressesPerso, ajouter une colonne "GPS Lieu de travail" et une colonne "Lieu de travail
- dans ces nouvelles colonnes, appliquer la fonction RECHERCHEV sur le matricule pour récupérer la position GPS du lieu de travail de l'agent et son lieu de travail
- trier sur la colonne Lieu de travail pour afficher les valeurs #N/A en premier (signifie que le matricule de la ligne courante n'a pas été trouvé dans le fichier des lieux de travail)

32 agents semblent ne pas se trouver dans le fichier Lieu de travail. J'arrive à en récupérer 4 : 1 que je sais être en retraite et les autres sont affectés à une unité hébergée sur le centre de Toulouse mais sont localisés sur d'autres centres.

Au final, nous avons 1379 agents avec une adresse personnelle et un lieu de travail sur le centre de Toulouse.
Ce fichier est prêt à être importé dans QGIS.

## Création du fichier des lieux de travail
- ouvrir le fichier modèle excel Ressources\DonneesAgents\LocalisationSites.xlsx
- à partir du fichier précédent, copier les lieux de travail (onglet AdressesPro) dans ce fichier, dans la colonne "Lieux de tr"
- supprimer tous les doublons (Données > Supprimer les doublons)
- pour chaque site, ajouter les positions GPS, vous pouvez utiliser https://nominatim.openstreetmap.org, :
    - faire votre recherche par adresse ou via la carte
    - cliquer sur "Show map bounds" en haut à droite de la carte  pour accéder aux coordonnées GPS de votre clic) 
**Essayer de prendre un point GPS du site qui soit ouvert à la circulation publique, sous peine de ne pouvoir calculer certains indicateurs d'accessibilité du site**  
e.g. pour le site de INRAE de Castanet-Tolosan   
![Point GPS site Castanet](images/GPS_LieuxTravail.png)  
- supprimer les lignes ne contenant pas de lieux de travail (le champs fid doit être renseigné pour tous les lieux de travail)


32 agents semblent ne pas se trouver dans le fichier Lieu de travail. J'arrive à en récupérer 4 : 1 que je sais être en retraite et les autres sont affectés à une unité hébergée sur le centre de Toulouse mais sont localisés sur d'autres centres.

Au final, nous avons 1379 agents avec une adresse personnelle et un lieu de travail sur le centre de Toulouse.
Ce fichier est prêt à être importé dans QGIS.

## Création de la cartographie avec QGIS

- ouvrir QGIS 3.28 Firenze (Long Term Release)
- ouvrir le projet PDME_INRAE.qgz (contient les découpes administratives des communes et des départements) + les groupes pour recevoir les couches à afficher
- ajouter la couche correspondant aux données des agents
    - enregistrer l'onglet AdressesPro sous le format txt (séparateur tabulation) avec comme nom FusionAdressesPersoEtPro.txt
    - Dans QGIS : 
        - sélectionner le groupe Données agents INRAE
        - cliquer sur le menu Couches > Ajouter une couche > Ajouter une couche de texte délimité...
        - Dans le champ "Nom de fichier", aller chercher le fichier FusionAdressesPersoEtPro.txt précédemment créé et compléter le formulaire comme suit

![Config import couche texte délimité](images/ajoutCoucheTexte.png)

        - Après validation, on obtient le positionnement des agents sur la carte

![Couche agents](images/coucheAgents.png)

On constate que les adresses personnelles sont éclatées partout en France, correspondant sans doute à des stagiaires ou des adresses de personnels non mises à jour. On va suprimer les points qui nous semblent aberrants
- sélectionner la couche "FusionAdressesPersoEtPro"
- sélectionner l'outil "Sélectionner des entités avec un polygone"

![Sélection entités polygone](images/filtreAgentsSelection.png)

- sélectionner le menu "Editer" > "Copier les entités" puis "Editer" > "Coller les entités comme" > "Nouvelle couche vecteur...."  
![Copier coller les entites dans nouvelle couche](images/copierCollerEntitesNouvelleCouche.png) 

On obtient la couche agentsAdressesProcheOccitanie (ne contient plus que 1204 agents!) : 
![Couche agents](images/coucheAgentsApresFiltre.png)

Le centre de Toulouse
### Import des données d'infrastructure

QGIS permet d'importer des données existantes sous différents formats. J'utilise principalement le format shapefile (shp) ou texte délimité (e.g. pour des points isolés comme des arrêts de bus/métro/train, stations d'autopartage....)

- infrastructure vélo
    - aménagements cylables, export openstreetmap : [https://transport.data.gouv.fr/datasets/amenagements-cyclables-france-metropolitaine](https://transport.data.gouv.fr/datasets/amenagements-cyclables-france-metropolitaine) - **présent dans le modèle de cartographie PDME INRAE**
- Transport en commun (car, bus, tram, metro)
    - Tisseo (contact avec conseiller mobilité)
    - Région Lio : lignes de cars [https://data.laregion.fr/explore/dataset/lignes-du-reseau-lio/information/?disjunctive.route_long_name](https://data.laregion.fr/explore/dataset/lignes-du-reseau-lio/information/?disjunctive.route_long_name)
    - Région Lio : arrêts de cars [https://data.laregion.fr/explore/dataset/arrets-du-reseau-lio/information/](https://data.laregion.fr/explore/dataset/arrets-du-reseau-lio/information/)
- SNCF : 
    - voies ferrées [https://data.sncf.com/explore/dataset/formes-des-lignes-du-rfn/export/](https://data.sncf.com/explore/dataset/formes-des-lignes-du-rfn/export/) **présent dans le modèle de cartographie PDME INRAE**
    - gares de voyageurs [https://data.sncf.com/explore/dataset/referentiel-gares-voyageurs/export/](https://data.sncf.com/explore/dataset/referentiel-gares-voyageurs/export/) **présent dans le modèle de cartographie PDME INRAE**
- Voiture : 
    - Aires de covoiturage [https://www.data.gouv.fr/fr/datasets/base-nationale-des-lieux-de-covoiturage/](https://www.data.gouv.fr/fr/datasets/base-nationale-des-lieux-de-covoiturage/) **présent dans le modèle de cartographie PDME INRAE**
    - Stations d'autopartage


### Sélection des entités desservant un site
Exemple des lignes de bus LIO desservant le site d'Auzeville, et des arrêts associés à ces lignes
L'ensemble des lignes de bus régionaux et des arrêts se trouvent dans 2 couches différentes  
![Couches LIO](images/TraitementGeneralLIO.png)

L'objectif est de sélectionner les lignes d'intérêt pour le site d'Auzeville : lignes 303, 350 et 383.
Pour réaliser cele, on va devoir filtrer ces lignes dans la couche : 
clic droit sur la couche lignes-du-reseau-lio > Filtrer   
Dans le champs "Expression de filtrage spécifique au fournisseur de données", mettre "route_id"='383'  OR "route_id"='303' OR "route_id"='350' (à adpater en fonction de votre source de données  
![Filtrage d'objets](images/TraitementGeneralLIO-filtrerLignes.png)  
Les lignes 303, 350 et 383 seules sont maintenant affichées, avec l'ensembles des arrêts LIO  
![Lignes filtrées](images/TraitementGeneralLIO-LignesFiltrees.png) 

Il s'agit maintenant de sélectionner les arrêts correspondants à ces trois lignes
Sélectionner la couche "Arrêts Lio" 
Dans le menu Traitements > Boîtes à ouils de traitement, recherche par mot clé le traitement "Sélectionner dans un rayon" 
Configurer comme suit puis Exécuter  
![Configuration sélection dans un rayon](images/TraitementGeneralSelectionnerDansRayon.png)    
Les arrêts LIO dans un rayon de 50m des lignes de bis LIO 303, 350 et 383 sont maintenant sélectionnés, il faut les enregistrer dans une nouvelle couche :  
- clic droit sur la couche "Arrêts Lio"    
- Exporter > "Sauvegarder les entités sélectionnées sous..."   
![Enregistrer les arrêts sélectionnés](images/TraitementGeneralExporterSelectionDansCouche.png)  
![Configuration de la couche vectorielle et nommage](TraitementGeneralExporterSelectionDansCoucheConfig.png)  


### Détermination du nombre d'agent à moins de 500m d'un arrêt de bus LIO des lignes 303, 350 et 383
Il faut d'abord détermner un espace tampon de 500m autour des arrêts de bus sélectionnés juste avant : 
- sélectionner la couche "ArretsLioLignes303-350-383" 
- Dans le menu Traitements > Boîtes à ouils de traitement, recherche par mot clé le traitement "Tampon"
- Configurer la distance à 500m > Exécuter
[Tampons arrêts LIO](images/TraitementTamponPointsArretsLio.png)   
- La couche temporaire créée doit être traitée pour regrouper tous les objets
- Dans le menu Traitements > Boîtes à ouils de traitement, recherche par mot clé le traitement "Regrouper"
