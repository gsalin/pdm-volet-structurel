# PDM-Volet-Structurel

TRAVAIL EN COURS, contact : gerald.salin@inrae.fr

## Objectif

Ce projet vise à guider les personnes souhaitant réaliser le volet structurel d'un plan de mobilité INRAE, avec notamment la mise à disposition d'un 
- modèle de fichier excel avec macro pour transformer les adresses des agents en points GPS, à importer dans un outil de cartographie
- modèle de cartographie sous QGIS pour faire une analyse des moyens de transport accessibles aux agents.

Ci-dessous un exemple de visualisation qu'on peut obtenir : 
- répartition géographique des agents travaillant sur un site donné : 
![Répartition géographique anonymisée des agents](images/RepartitionAgentsCastanet.png)
- représentation des différentes infrastructures de transport à proximité d'un site donné (l'affichage est ici trop chargé voontairement afin de montrer les différentes possibilités qu'offre QGIS)
![Représentation des différentes infrastructures de transport](images/ExempleCartographie.png)

## Pré-requis

- accès au logiciel QGIS
- accès aux adresses personnelles des agents, savoir suivi le guide PDM INRAE, notamment pour tout ce qui concerne le RGPD
- accès aux différentes données à représenter sur la cartographie (fond de carte, adresses personnelles des agents, adresse des sites INRAE où travaillent les agents, pistes cyclables, location de vélo, services vélo, parking véos, lignes + arrêt de bus/métro/tram, lignes + arrêts de train, stations d'autopartage, aires covoiturage...)
- accès au projet [https://forgemia.inra.fr/gsalin/pdm-volet-structurel](https://forgemia.inra.fr/gsalin/pdm-volet-structurel) pour récupérer le projet modèle QGIS


## Obtention des ressources

- QGIS peut être téléchargé ici : [https://qgis.org/en/site/](https://qgis.org/en/site/). La documentation de QGIS est accessible [ici](https://www.qgis.org/en/docs/index.html)
- Obtention des adresses personnelles des agents, après les avoir informé que leurs données personnelles allaient être utilisées (cf guide PDM + Fiche référentiel_PDM_volet_structurel)
  - Demander au service "administration locale du personnel" qu'ils réalisent l'extraction (il semble que SI RH ne permette pas d'avoir tout en une seule extraction)
    - Extraire la liste des adresses personnelles (matricule, nom, prénom, adresse personnelle)
    - Extraire la liste des lieux de travail (matricule, lieu de travail, date d'entrée (pour permettre de supprimer les doublons de localisation en ne conservant que la dernière localisation))
  - L'envoi de ces fichiers doit se faire de façon sécurisée en utilisant [filesender](https://filesender.renater.fr/) en chiffrant les fichiers, au seul destinataire INRAE, responsable traitement
  - S'assurer que les lignes correspondant aux personnes ayant exprimé leur refus que leurs données personnelles soient utilisées ont bien été supprimées du fichier
- récupération du modèle de projet PDM sous QGIS depuis la page [https://forgemia.inra.fr/gsalin/pdm-volet-structurel](https://forgemia.inra.fr/gsalin/pdm-volet-structurel) > Télécharger comme fichier zip
![download comme zip](images/forgemiaDownloadProjet.png){width=600}
- Obtention des données nécessaires à la cartographie des infrastructures
    - une majorité des données nécessaires pour la réalisation du volet structurel est accessible en open data.
 

## Traitement des données

- Données des agents : 
    - Vérifier la cohérence des données 
      - Vérifier qu'il n'y a pas de doublons Matricule dans le fichier avec les adresses personnelles
      - Vérifier qu'il n'y a pas de doublons Matricule dans le fichier avec la localisation du site de travail. Dans ce cas, utiliser l'information "Date d'entrée" pour ne sélectionner que la ligne correspondant à la dernière localisation de travail de l'agent
    -  transformer les adresses personnelles en point GPS (pour pouvoir les représenter automatiquement sur une carte QGIS)
      - Charger le contenu du fichier avec les adresses personnelles dans le fichier AgentsNominatim.xlsm
      - faire en sorte d'avoir 3 colonnes avec le numéro et le nom de la rue, le code postal puis la commune
      - ouvrir le fichier AgentsNominatim.xlsm > Développeur > Visual Basic > Modules > Module 1  
      

## Traitement par année
- [Toulouse 2023](Toulouse/2023/analyse.md) (mise en place de la procédure pour créer la macro et préparer le modèle de projet de cartographie)




