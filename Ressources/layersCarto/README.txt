Ressource:Source
communes-20220101-shp.zip:https://www.data.gouv.fr/fr/datasets/decoupage-administratif-communal-francais-issu-d-openstreetmap/
Tisseo:Tisseo
departements-20180101-shp.zip:https://www.data.gouv.fr/fr/datasets/contours-des-departements-francais-issus-d-openstreetmap/
OSM/midi-pyrenees-latest-free.shp:http://download.geofabrik.de/europe/france/midi-pyrenees.html